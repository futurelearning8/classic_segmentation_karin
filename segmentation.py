import numpy as np
import cv2
from os import path
from matplotlib import pyplot as plt


def check_path(file_path):
    if not path.exists(file_path):
        print("No such file!")
        exit()


def main():
    data_path = '../data/'
    # Load images
    platelets_image_path = data_path + "platelets_q.jpg"
    platelets_image = cv2.imread(platelets_image_path)

    # thresholds where chosen according LAB
    platelets_image_lab = cv2.cvtColor(platelets_image, cv2.COLOR_BGR2LAB)

    # Threshold
    high_th = np.array([190, 128, 128])
    mid_th = np.array([125, 128, 128])
    low_th = np.array([0, 128, 128])

    # Red
    mask_red = cv2.inRange(platelets_image_lab, mid_th, high_th)
    kernel = np.ones((9, 9), np.uint8)
    mask_red_opening = cv2.morphologyEx(mask_red, cv2.MORPH_OPEN, kernel)
    mask_red_erosion = cv2.erode(mask_red_opening, kernel, iterations=2)
    mask_red_dilate = cv2.dilate(mask_red_erosion, kernel, iterations=1)

    # draw red cells on figure according mask
    red_image = np.zeros(platelets_image.shape, np.uint8)
    red_image[:] = (255, 0, 0)
    r = cv2.bitwise_or(red_image, red_image, mask=mask_red_dilate)

    # Purple
    mask_purple = cv2.inRange(platelets_image_lab, low_th, mid_th)
    kernel = np.ones((5, 5), np.uint8)
    mask_purple_opening = cv2.morphologyEx(mask_purple, cv2.MORPH_OPEN, kernel)
    mask_purple_dilate = cv2.dilate(mask_purple_opening, kernel, iterations=1)

    # draw purple cells on figure according mask
    purple_image = np.zeros(platelets_image.shape, np.uint8)
    purple_image[:] = (102, 0, 204)
    p = cv2.bitwise_or(purple_image, purple_image, mask=mask_purple_dilate)

    # connectedComponents stats for counting and diameter
    (numLabels_r, _, stats_r, _) = cv2.connectedComponentsWithStats(mask_red_dilate, 4, cv2.CV_32S)
    print(f'Number of Red cells is: {numLabels_r}')

    r_mean_diameter = np.mean(stats_r[:, cv2.CC_STAT_HEIGHT])
    print(f'Mean Diameter for Red cells is: {r_mean_diameter}\n')

    (numLabels_p, _, stats_p, _) = cv2.connectedComponentsWithStats(mask_purple_dilate, 4, cv2.CV_32S)
    print(f'Number of Red cells is: {numLabels_p}')

    p_mean_diameter = np.mean(stats_p[:, cv2.CC_STAT_HEIGHT])
    print(f'Mean Diameter for Red cells is: {p_mean_diameter}')

    text_stats = f"Count: Red - {numLabels_r}, Purple - {numLabels_p}\n" \
                 f"Mean Diameter: Red - {r_mean_diameter}, Purple - {p_mean_diameter}\n"

    # output image
    mask = cv2.bitwise_or(r, p)
    plt.imshow(mask)
    plt.text(0, 0, text_stats)
    plt.savefig('../data/Segmentation-Output.png')
    plt.show()


if __name__ == "__main__":
    main()
